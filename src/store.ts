import { InjectionKey } from 'vue'
import { createStore, ActionContext, useStore as baseUseStore, Store } from 'vuex'
import data from './sampleData'

export type Row = { [key: string]: string }

export interface Column {
  name: string
}

export interface Document {
  data: Row[],
  columns: Column[],
}

export interface Selection {
  row: number,
  col: number,
}

export interface Editor {
  doc: string,
  selection: Selection,
}

export interface State {
  docs: { [key: string]: Document }
  editors: { [key: string]: Editor }
}

export type Direction = 'up' | 'down' | 'left' | 'right'

export const key: InjectionKey<Store<State>> = Symbol()

const columns = Object.keys(data[0]).map(name => ({name}))

const state: State = {
  docs: {
    main: {
      data,
      columns,
    },
  },
  editors: {
    main: {
      doc: 'main',
      selection: {
        row: 0,
        col: 0,
      },
    },
  },
}

const getters = {

}

const actions = {
  setSelectedPosition({commit}: ActionContext<State, State>, {editor, row, col}: {editor: string, row: number, col: number}) {
    commit('setSelectedRow', {editor, value: row})
    commit('setSelectedColumn', {editor, value: col})
  },
  move({state, commit}: ActionContext<State, State>, {editor, direction}: {editor: string, direction: Direction}) {
    const y = {up: -1, down: 1, left: 0, right: 0}[direction]
    const x = {left: -1, right: 1, up: 0, down: 0}[direction]
    const {data, columns} = state.docs[state.editors[editor].doc]
    if (y) {
      let row = state.editors[editor].selection.row + y
      if (row < 0) {
        row = data.length - 1
      } else if (row > data.length - 1) {
        row = 0
      }
      commit('setSelectedRow', {editor, value: row})
    }
    if (x) {
      let col = state.editors[editor].selection.col + x
      if (col < 0) {
        col = columns.length - 1
      } else if (col > columns.length - 1) {
        col = 0
      }
      commit('setSelectedColumn', {editor, value: col})
    }
  },
}

const mutations = {
  setSelectedRow(state: State, {editor, value}: {editor: string, value: number}) {
    state.editors[editor].selection.row = value
  },
  setSelectedColumn(state: State, {editor, value}: {editor: string, value: number}) {
    state.editors[editor].selection.col = value
  },
}

export function useStore() {
  return baseUseStore(key)
}

export const store = createStore<State>({
  state,
  getters,
  actions,
  mutations,
})