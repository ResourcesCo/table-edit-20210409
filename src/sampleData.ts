const data = [
    {
      "rank": "1",
      "name": "New York City-Newark-Jersey City, NY-NJ-PA MSA",
      "estimate2019": "19,216,182",
      "census2010": "18,897,109",
      "percentChange": "+1.69%",
      "encompasingCSA": "New York-Newark, NY-NJ-CT-PA CSA"
    },
    {
      "rank": "2",
      "name": "Los Angeles-Long Beach-Anaheim, CA MSA",
      "estimate2019": "13,214,799",
      "census2010": "12,828,837",
      "percentChange": "+3.01%",
      "encompasingCSA": "Los Angeles-Long Beach, CA CSA"
    },
    {
      "rank": "3",
      "name": "Chicago-Naperville-Elgin, IL-IN-WI MSA",
      "estimate2019": "9,458,539",
      "census2010": "9,461,105",
      "percentChange": "−0.03%",
      "encompasingCSA": "Chicago-Naperville, IL-IN-WI CSA"
    },
    {
      "rank": "4",
      "name": "Dallas-Fort Worth-Arlington, TX MSA",
      "estimate2019": "7,573,136",
      "census2010": "6,366,542",
      "percentChange": "+18.95%",
      "encompasingCSA": "Dallas-Fort Worth, TX-OK CSA"
    },
    {
      "rank": "5",
      "name": "Houston-The Woodlands-Sugar Land, TX MSA",
      "estimate2019": "7,066,141",
      "census2010": "5,920,416",
      "percentChange": "+19.35%",
      "encompasingCSA": "Houston-The Woodlands, TX CSA"
    },
    {
      "rank": "6",
      "name": "Washington-Arlington-Alexandria, DC-VA-MD-WV MSA",
      "estimate2019": "6,280,487",
      "census2010": "5,649,540",
      "percentChange": "+11.17%",
      "encompasingCSA": "Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA"
    },
    {
      "rank": "7",
      "name": "Miami-Fort Lauderdale-West Palm Beach, FL MSA",
      "estimate2019": "6,166,488",
      "census2010": "5,564,635",
      "percentChange": "+10.82%",
      "encompasingCSA": "Miami-Port St. Lucie-Fort Lauderdale, FL CSA"
    },
    {
      "rank": "8",
      "name": "Philadelphia-Camden-Wilmington, PA-NJ-DE-MD MSA",
      "estimate2019": "6,102,434",
      "census2010": "5,965,343",
      "percentChange": "+2.30%",
      "encompasingCSA": "Philadelphia-Reading-Camden, PA-NJ-DE-MD CSA"
    },
    {
      "rank": "9",
      "name": "Atlanta-Sandy Springs-Alpharetta, GA MSA",
      "estimate2019": "6,020,364",
      "census2010": "5,286,728",
      "percentChange": "+13.88%",
      "encompasingCSA": "Atlanta–Athens-Clarke County–Sandy Springs, GA-AL CSA"
    },
    {
      "rank": "10",
      "name": "Phoenix-Mesa-Chandler, AZ MSA",
      "estimate2019": "4,948,203",
      "census2010": "4,192,887",
      "percentChange": "+18.01%",
      "encompasingCSA": "Phoenix-Mesa, AZ CSA"
    },
    {
      "rank": "11",
      "name": "Boston-Cambridge-Newton, MA-NH MSA",
      "estimate2019": "4,873,019",
      "census2010": "4,552,402",
      "percentChange": "+7.04%",
      "encompasingCSA": "Boston-Worcester-Providence, MA-RI-NH-CT CSA"
    },
    {
      "rank": "12",
      "name": "San Francisco-Oakland-Berkeley, CA MSA",
      "estimate2019": "4,731,803",
      "census2010": "4,335,391",
      "percentChange": "+9.14%",
      "encompasingCSA": "San Jose-San Francisco-Oakland, CA CSA"
    },
    {
      "rank": "13",
      "name": "Riverside-San Bernardino-Ontario, CA MSA",
      "estimate2019": "4,650,631",
      "census2010": "4,224,851",
      "percentChange": "+10.08%",
      "encompasingCSA": "Los Angeles-Long Beach, CA CSA"
    },
    {
      "rank": "14",
      "name": "Detroit–Warren–Dearborn, MI MSA",
      "estimate2019": "4,319,629",
      "census2010": "4,296,250",
      "percentChange": "+0.54%",
      "encompasingCSA": "Detroit-Warren-Ann Arbor, MI CSA"
    },
    {
      "rank": "15",
      "name": "Seattle-Tacoma-Bellevue, WA MSA",
      "estimate2019": "3,979,845",
      "census2010": "3,439,809",
      "percentChange": "+15.70%",
      "encompasingCSA": "Seattle-Tacoma, WA CSA"
    },
    {
      "rank": "16",
      "name": "Minneapolis-St. Paul-Bloomington, MN-WI MSA",
      "estimate2019": "3,654,908",
      "census2010": "3,346,859",
      "percentChange": "+9.20%",
      "encompasingCSA": "Minneapolis-St. Paul, MN-WI CSA"
    },
    {
      "rank": "17",
      "name": "San Diego-Chula Vista-Carlsbad, CA MSA",
      "estimate2019": "3,338,330",
      "census2010": "3,095,313",
      "percentChange": "+7.85%",
      "encompasingCSA": ""
    },
    {
      "rank": "18",
      "name": "Tampa-St. Petersburg-Clearwater, FL MSA",
      "estimate2019": "3,194,831",
      "census2010": "2,783,243",
      "percentChange": "+14.79%",
      "encompasingCSA": ""
    },
    {
      "rank": "19",
      "name": "Denver-Aurora-Lakewood, CO MSA",
      "estimate2019": "2,967,239",
      "census2010": "2,543,482",
      "percentChange": "+16.66%",
      "encompasingCSA": "Denver-Aurora, CO CSA"
    },
    {
      "rank": "20",
      "name": "St. Louis, MO-IL MSA",
      "estimate2019": "2,803,228",
      "census2010": "2,787,701",
      "percentChange": "+0.56%",
      "encompasingCSA": "St. Louis-St. Charles-Farmington, MO-IL CSA"
    },
    {
      "rank": "21",
      "name": "Baltimore-Columbia-Towson, MD MSA",
      "estimate2019": "2,800,053",
      "census2010": "2,710,489",
      "percentChange": "+3.30%",
      "encompasingCSA": "Washington-Baltimore-Arlington, DC-MD-VA-WV-PA CSA"
    },
    {
      "rank": "22",
      "name": "Charlotte-Concord-Gastonia, NC-SC MSA",
      "estimate2019": "2,636,883",
      "census2010": "2,243,960",
      "percentChange": "+17.51%",
      "encompasingCSA": "Charlotte-Concord, NC-SC CSA"
    },
    {
      "rank": "23",
      "name": "Orlando-Kissimmee-Sanford, FL MSA",
      "estimate2019": "2,608,147",
      "census2010": "2,134,411",
      "percentChange": "+22.20%",
      "encompasingCSA": "Orlando-Lakeland-Deltona, FL CSA"
    },
    {
      "rank": "24",
      "name": "San Antonio-New Braunfels, TX MSA",
      "estimate2019": "2,550,960",
      "census2010": "2,142,508",
      "percentChange": "+19.06%",
      "encompasingCSA": ""
    },
    {
      "rank": "25",
      "name": "Portland-Vancouver-Hillsboro, OR-WA MSA",
      "estimate2019": "2,492,412",
      "census2010": "2,226,009",
      "percentChange": "+11.97%",
      "encompasingCSA": "Portland-Vancouver-Salem, OR-WA CSA"
    },
    {
      "rank": "26",
      "name": "Sacramento-Roseville-Folsom, CA MSA",
      "estimate2019": "2,363,730",
      "census2010": "2,149,127",
      "percentChange": "+9.99%",
      "encompasingCSA": "Sacramento-Roseville, CA CSA"
    },
    {
      "rank": "27",
      "name": "Pittsburgh, PA MSA",
      "estimate2019": "2,317,600",
      "census2010": "2,356,285",
      "percentChange": "−1.64%",
      "encompasingCSA": "Pittsburgh-New Castle-Weirton, PA-OH-WV CSA"
    },
    {
      "rank": "28",
      "name": "Las Vegas-Henderson-Paradise, NV MSA",
      "estimate2019": "2,266,715",
      "census2010": "1,951,269",
      "percentChange": "+16.17%",
      "encompasingCSA": "Las Vegas-Henderson, NV-AZ CSA"
    },
    {
      "rank": "29",
      "name": "Austin-Round Rock-Georgetown, TX MSA",
      "estimate2019": "2,227,083",
      "census2010": "1,716,289",
      "percentChange": "+29.76%",
      "encompasingCSA": ""
    },
    {
      "rank": "30",
      "name": "Cincinnati, OH-KY-IN MSA",
      "estimate2019": "2,221,208",
      "census2010": "2,137,667",
      "percentChange": "+3.91%",
      "encompasingCSA": "Cincinnati-Wilmington-Maysville, OH-KY-IN CSA"
    },
    {
      "rank": "31",
      "name": "Kansas City, MO-KS MSA",
      "estimate2019": "2,157,990",
      "census2010": "2,009,342",
      "percentChange": "+7.40%",
      "encompasingCSA": "Kansas City-Overland Park-Kansas City, MO-KS CSA"
    },
    {
      "rank": "32",
      "name": "Columbus, OH MSA",
      "estimate2019": "2,122,271",
      "census2010": "1,901,974",
      "percentChange": "+11.58%",
      "encompasingCSA": "Columbus-Marion-Zanesville, OH CSA"
    },
    {
      "rank": "33",
      "name": "Indianapolis-Carmel-Anderson, IN MSA",
      "estimate2019": "2,074,537",
      "census2010": "1,887,877",
      "percentChange": "+9.89%",
      "encompasingCSA": "Indianapolis-Carmel-Muncie, IN CSA"
    },
    {
      "rank": "34",
      "name": "Cleveland-Elyria, OH MSA",
      "estimate2019": "2,048,449",
      "census2010": "2,077,240",
      "percentChange": "−1.39%",
      "encompasingCSA": "Cleveland-Akron-Canton, OH CSA"
    },
    {
      "rank": "35",
      "name": "San Jose-Sunnyvale-Santa Clara, CA MSA",
      "estimate2019": "1,990,660",
      "census2010": "1,836,911",
      "percentChange": "+8.37%",
      "encompasingCSA": "San Jose-San Francisco-Oakland, CA CSA"
    },
    {
      "rank": "36",
      "name": "Nashville-Davidson–Murfreesboro–Franklin, TN MSA",
      "estimate2019": "1,934,317",
      "census2010": "1,646,200",
      "percentChange": "+17.50%",
      "encompasingCSA": "Nashville-Davidson–Murfreesboro, TN CSA"
    },
    {
      "rank": "37",
      "name": "Virginia Beach-Norfolk-Newport News, VA-NC MSA",
      "estimate2019": "1,768,901",
      "census2010": "1,713,954",
      "percentChange": "+3.21%",
      "encompasingCSA": "Virginia Beach-Norfolk, VA-NC CSA"
    },
    {
      "rank": "38",
      "name": "Providence-Warwick, RI-MA MSA",
      "estimate2019": "1,624,578",
      "census2010": "1,600,852",
      "percentChange": "+1.48%",
      "encompasingCSA": "Boston-Worcester-Providence, MA-RI-NH-CT CSA"
    },
    {
      "rank": "39",
      "name": "Milwaukee-Waukesha, WI MSA",
      "estimate2019": "1,575,179",
      "census2010": "1,555,908",
      "percentChange": "+1.24%",
      "encompasingCSA": "Milwaukee-Racine-Waukesha, WI CSA"
    },
    {
      "rank": "40",
      "name": "Jacksonville, FL MSA",
      "estimate2019": "1,559,514",
      "census2010": "1,345,596",
      "percentChange": "+15.90%",
      "encompasingCSA": "Jacksonville-St. Marys-Palatka, FL-GA CSA"
    },
    {
      "rank": "41",
      "name": "Oklahoma City, OK MSA",
      "estimate2019": "1,408,950",
      "census2010": "1,252,987",
      "percentChange": "+12.45%",
      "encompasingCSA": "Oklahoma City-Shawnee, OK CSA"
    },
    {
      "rank": "42",
      "name": "Raleigh-Cary, NC MSA",
      "estimate2019": "1,390,785",
      "census2010": "1,130,490",
      "percentChange": "+23.02%",
      "encompasingCSA": "Raleigh-Durham-Cary, NC CSA"
    },
    {
      "rank": "43",
      "name": "Memphis, TN-MS-AR MSA",
      "estimate2019": "1,346,045",
      "census2010": "1,316,100",
      "percentChange": "+2.28%",
      "encompasingCSA": "Memphis-Forrest City, TN-MS-AR CSA"
    },
    {
      "rank": "44",
      "name": "Richmond, VA MSA",
      "estimate2019": "1,291,900",
      "census2010": "1,186,501",
      "percentChange": "+8.88%",
      "encompasingCSA": ""
    },
    {
      "rank": "45",
      "name": "New Orleans-Metairie, LA MSA",
      "estimate2019": "1,270,530",
      "census2010": "1,189,866",
      "percentChange": "+6.78%",
      "encompasingCSA": "New Orleans-Metairie-Hammond, LA-MS CSA"
    },
    {
      "rank": "46",
      "name": "Louisville/Jefferson County, KY-IN MSA",
      "estimate2019": "1,265,108",
      "census2010": "1,202,718",
      "percentChange": "+5.19%",
      "encompasingCSA": "Louisville/Jefferson County–Elizabethtown–Bardstown, KY-IN CSA"
    },
    {
      "rank": "47",
      "name": "Salt Lake City, UT MSA",
      "estimate2019": "1,232,696",
      "census2010": "1,087,873",
      "percentChange": "+13.31%",
      "encompasingCSA": "Salt Lake City-Provo-Orem, UT CSA"
    },
    {
      "rank": "48",
      "name": "Hartford-East Hartford-Middletown, CT MSA",
      "estimate2019": "1,204,877",
      "census2010": "1,212,381",
      "percentChange": "−0.62%",
      "encompasingCSA": "Hartford-East Hartford, CT CSA"
    },
    {
      "rank": "49",
      "name": "Buffalo-Niagara Falls, NY MSA",
      "estimate2019": "1,127,983",
      "census2010": "1,135,509",
      "percentChange": "−0.66%",
      "encompasingCSA": "Buffalo-Cheektowaga-Cattaraugus, NY CSA"
    },
    {
      "rank": "50",
      "name": "Birmingham-Hoover, AL MSA",
      "estimate2019": "1,090,435",
      "census2010": "1,061,024",
      "percentChange": "+2.77%",
      "encompasingCSA": "Birmingham-Hoover-Talladega, AL CSA"
    }
  ]

  export default data